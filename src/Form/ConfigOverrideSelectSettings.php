<?php

namespace Drupal\config_override_select\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SettingsForm.
 *
 * @package Drupal\config_override_select\Form
 */
class ConfigOverrideSelectSettings extends ConfigFormBase {

  /**
   * Get the App Root.
   *
   * @var string|\SplString
   */
  protected $appRoot;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'config_override_select.settings',
    ];
  }

  /**
   * Class constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory, $app_root) {
    parent::__construct($config_factory);
    $this->appRoot = $app_root;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('app.root')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('config_override_select.settings');

    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $config->get('enabled'),
    ];

    $form['config_folders'] = [
      '#type' => 'details',
      '#title' => $this->t('Config Override directories'),
      '#open' => TRUE,
      '#tree' => TRUE,
      '#description' => $this->t('Specify the path to the folders you will switch between. Note the base path used is @base', [
        '@base' => $this->appRoot,
      ]),
      '#states' => [
        'visible' => [
          ':input[name="enabled"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    foreach ($config->get('config_folders') as $key => $value) {
      $form['config_folders'][$key] = [
        '#type' => 'textfield',
        '#size' => 30,
        '#default_value' => $value,
      ];
    }

    $form['config'] = [
      '#type' => 'details',
      '#title' => $this->t('Current override configuration'),
      '#open' => TRUE,
      '#tree' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="enabled"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    $form['config']['active_config'] = [
      '#type' => 'radios',
      '#title' => $this->t('Active configuration'),
      '#options' => array_filter($config->get('config_folders')),
      '#default_value' => $config->get('active_config'),
      '#description' => $this->t('Only one config override can be used at a time.'),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('config_override_select.settings');
    if ($form_state->getValue(['enabled']) === 1) {
      $config
        ->set('enabled', $form_state->getValue(['enabled']))
        ->set('active_config', $form_state->getValue(['config', 'active_config']));

      // For now we are fixing at 5 values, but in the future it may be dynamic.
      for ($i = 0; $i < 5; $i++) {
        $config->set('config_folders.' . $i . '', $form_state->getValue(['config_folders', $i]));
      }
    }
    else {
      $config
        ->set('enabled', $form_state->getValue(['enabled']));
    }

    $config->save();
    drupal_set_message('Configuration saved.');
  }

}
