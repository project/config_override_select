<?php

namespace Drupal\config_override_select\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Exception;
use Symfony\Component\Yaml\Yaml;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Configuration override service.
 */
class ConfigOverrideSelectService implements ConfigOverrideSelectInterface {
  use StringTranslationTrait;
  /**
   * The parsed YAML files will be placed into this array.
   *
   * @var array
   */
  protected $overrideData = [];

  /**
   * ConfigFactory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * Stores the currently processing file name.
   *
   * @var string
   */
  protected $currentFile;

  /**
   * ConfigOverrideSelectService constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config Factory.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   String Translation Interface.
   * @param string $app_root
   *   Drupal Root.
   */
  public function __construct(ConfigFactoryInterface $config_factory, TranslationInterface $string_translation, $app_root) {
    $this->stringTranslation = $string_translation;
    $this->appRoot = $app_root;
    $this->config = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function fetchOverrides() {
    // @TODO caching.
    return $this->loadConfig();
  }

  /**
   * Loads configuration overrides.
   *
   * @return array|\Drupal\config_override_select\Service\overrideData
   *   Returns an array of all config overrides found in directory.
   */
  private function loadConfig() {
    // Load the Config Override Select settings.
    $config = $this->config->get('config_override_select.settings');

    // Ensure the user has enabled Config Override Select.
    if ($config->get('enabled')) {
      // Use the specified directory.
      $active_config_id = $config->get('active_config');
      $active_config = $config->get('config_folders.' . $active_config_id);

      // Scan the selected folder for all YML files.
      $active_config_full_path = $this->appRoot . $active_config;
      if (is_dir($active_config_full_path)) {
        foreach (glob($active_config_full_path . "*.yml") as $filename) {
          // Use the filename to set the key for the config to override.
          $this->currentFile = basename($filename, '.yml');
          try {
            $file_data = [
              $this->currentFile => Yaml::parse(file_get_contents($filename)),
            ];
            // Combine results (for multiple files).
            $this->overrideData = array_merge($file_data, $this->overrideData);
          }
          catch (Exception $e) {
            if ($this->currentFile) {
              drupal_set_message($this->t('Config Override Select hit an error parsing @currentfile, returned: @error', [
                '@currentfile' => $this->currentFile,
                '@error' => $e->getMessage(),
              ]), 'error');
            }
            else {
              drupal_set_message($e->getMessage());
            }
          }
        }
      }
      else {
        drupal_set_message($this->t('Config Override Select is enabled, but directory specified is not valid. @directory', [
          '@directory' => $active_config_full_path,
        ]), 'warning');
      }
    }

    if (empty($this->overrideData)) {
      return NULL;
    }

    return $this->overrideData;
  }

}
