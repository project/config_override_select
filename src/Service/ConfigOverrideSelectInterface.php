<?php

namespace Drupal\config_override_select\Service;

/**
 * Provides an interface defining the Config Override Select service.
 */
interface ConfigOverrideSelectInterface {

  /**
   * Ensure the configuration is only loaded once.
   *
   * @return array|\Drupal\config_override_select\Service\overrideData
   *   Override data from user supplied config.
   */
  public function fetchOverrides();

}
