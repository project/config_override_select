<?php

namespace Drupal\config_override_select\Config;

use Drupal\config_override_select\Service\ConfigOverrideSelectInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryOverrideInterface;
use Drupal\Core\Config\StorageInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * Config Override Select.
 */
class ConfigOverrideSelectOverrides implements ConfigFactoryOverrideInterface {
  use StringTranslationTrait;
  /**
   * ConfigOverrideSelectService.
   *
   * @var \Drupal\config_override_select\Service\ConfigOverrideSelectInterface
   */
  protected $overrideService;

  /**
   * Overrides supplied by the service.
   *
   * @var array|\Drupal\config_override_select\Service\overrideData
   */
  protected $overrides;

  /**
   * ConfigOverrideSelectOverrides constructor.
   *
   * @param \Drupal\config_override_select\Service\ConfigOverrideSelectInterface $override_service
   *   Config Override Select Service.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   String Translation Interface.
   */
  public function __construct(ConfigOverrideSelectInterface $override_service, TranslationInterface $string_translation) {
    $this->stringTranslation = $string_translation;
    $this->overrideService = $override_service;
    $this->overrides = $this->overrideService->fetchOverrides();
  }

  /**
   * {@inheritdoc}
   */
  public function loadOverrides($names) {
    if ($this->overrides === NULL) {
      return [];
    }

    $overrides = [];
    foreach ($this->overrides as $key => $values) {
      if (in_array($key, $names)) {
        $overrides[$key] = $values;
      }
    }

    drupal_set_message($this->t('Config Override Select development module is currently overriding configuration values.'), 'warning');

    return $overrides;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheSuffix() {
    return 'ConfigOverrideSelectOverrider';
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata($name) {
    return new CacheableMetadata();
  }

  /**
   * {@inheritdoc}
   */
  public function createConfigObject($name, $collection = StorageInterface::DEFAULT_COLLECTION) {
    return NULL;
  }

}