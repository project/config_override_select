# Introduction

Config Override Select provides developers an easy way to override 
multiple configuration items across their entire site, including modules in
one place.

Developers can manage several configuration override files and folders to
allow greater flexibility when testing:

* Create a single YML file for each module to allow easier management
* Have different configuration files depending on the testing environment 
(e.g. dev, uat, feature branch)

##### THIS IS A DEVELOPMENT MODULE AND SHOULD NOT BE INSTALLED ON PRODUCTION.

# Installation

Download and install the module as you would with any other Drupal module.
If you're using composer, it's recommended to place this in require-dev to ensure
the module never reaches production.

* Enable the module in your Drupal admin interface.
* The configuration page for this module is in /admin/config/override/settings,
  and can be accessed in the admin menu under Configuration -> 
  Development -> Config Override Select.

# Configuration

## Getting Started and Basic Usage

Config Override Select directories can be specified on the configuration page.
The base path is DRUPAL_ROOT and will display in the description text of the field.
You can use relative paths, e.g. if the Drupal root is ``my/awesome/app``, you could use
``/../config/`` if your config is located in ``my/awesome/config/``. Once you
have entered the directory and selected the active override configuration,
you can store your files in the folder specified.

## YML Format

All YML files should be a config export from Drupal. The filename will be the 
configuration name to override, e.g.
``system.site.yml`` or ``core.entity_view_display.node.page.default.yml`` and will contain the
config export in the file with your edits:

```
uuid: ''
name: 'Drupal - DEVELOPER CONFIG OVERRIDE'
mail: ''
slogan: 'WARNING: DEV MODE.'
page:
  403: 'customdev403.html'
  404: 'customdev404.html'

...etc
```
